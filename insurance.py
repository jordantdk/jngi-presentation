import random
import time
from subprocess import call

policies=[]
extensions=["BURGLARY","MALICIOUS DAMAGE","PLATE GLASS","EXPLOSION","FLOOD(CAUSED BY BREAKAGE OR LEAKAGE OF PIPES","RIOT DAMAGE"]

main_options=["ADD POLICY","VIEW CLIENT","VIEW ALL CLIENTS","VIEW POLICY DETAILS","VIEW WARRANTIES","EXIT"]

def displayWarranties():
	file=open("warranties.txt","r")
	data= file.read()
	printlogo("logo.txt")
	print("\n\n\n\n",data)
	file.close()

def printlogo(filename):
	file=open(filename,'r')
	data=file.read()
	call("clear")
	print(data)

def aboutPolicy():
	file=open("fire and allied.txt","r")
	data=file.read()
	printlogo("logo.txt")
	print("\n\n",data)
	file.close()

def menu(options):
	printlogo("logo.txt")
	print("\n\n\n\t\t\t\t\t\tCHOOSE A NUMBER CORRESPONDING WITH THE OPTION YOU WANT TO SELECT")
	length=len(options)
	for opt in range(length):
		print("\n\t\t\t\t\t\t\t\t"+str(opt+1)+" ",options[opt]);
	option=int(input("\nENTER OPTION: "))
	return option


class Risk:
	def __init__(self):
		self.extensions=[]
		self.description=input("\n\t\t\t\t\t\t\tENTER THE RISK YOU WISH TO ADD:")
		self.grossPremium=float(input("\n\t\t\t\t\t\t\tENTER THE GROSS PREMIUM PAID:"))
		self.dateAdded=time.asctime( time.localtime(time.time()))
		self.addExtension()

	def addExtension(self):
		printlogo("logo.txt")
		length=len(extensions)
		while True:
			for i in range(length):
				if(extensions[i] not in self.extensions):
					print("\n\t\t\t\t\t\t\t\t"+str(i+1)+" ",extensions[i])
			print("\n\n\n\t\t\t\t\t\t\tSELECT THE NUMBER CORRESPONDING WITH THE EXTENSION YOU WANT TO ADD")
			try:
				choice=int(input())
			except:
				continue
			self.extensions.append(extensions[choice-1])
			print("DO YOU WANT TO EXIT? [Y/N]")
			while True:
				exit=input()
				exit=exit.upper()
				if(exit=='Y' or exit=='N'):break
				print("INVALID OPTION! TRY AGAIN:")
			if(exit=='Y'):break
		print("EXTENSIONS WERE ADDED SUCCESSFULLY.")
	def displayRiskDetails(self):
		print("\n\t\t\t\t\tDESCRIPTION: ",self.description)
		print("\n\t\t\t\t\tGROSS PREMIUM: ",self.grossPremium)
		print("\n\t\t\t\t\tDATE ADDED: ",self.dateAdded)
		print("\n\t\t\t\tEXTENSIONS:\n\t\t\t\t",self.extensions)

class policy:

	def __init__(self,insured,address):
		self.risks=[]
		self.policyname="Fire and Allide Policy"
		self.insured=insured
		self.address=address
		self.suminsured=float(input("\n\t\t\t\t\t\t\tENTER THE SUM INSURED:"))

	def setCurrency(self):
		while True:
			self.currency=input("\n\t\t\t\t\t\t\tENTER THE TYPE OF CURRENCY(JMD OR USD):")
			self.currency=self.currency.upper()
			if(self.currency=="JMD" or self.currency=="USD"):break
			print("THAT IS AN INVALID CURRENCY")
		print("\t\t\t\t\t\t\tCURRENCY WAS SET TO",self.currency+".")

	def setcontact(self):
		self.contact=input("\n\t\t\t\t\t\t\tENTER THE CONTACT FOR "+self.insured+": (876)")

	def addRisk(self):
		self.risk=Risk()
		self.risks.append(self.risk)

	def display(self):
		printlogo("logo.txt")
		print("\n\n\n\t\t\t\t\tPOLICY NAME: ",self.policyname)
		print("\n\t\t\t\t\tINSURED: ",self.insured)
		print("\n\t\t\t\t\tADDRESS: ",self.address)
		print("\n\t\t\t\t\tCURRENCY: ",self.currency)
		print("\n\t\t\t\t\tSUM INSURED: $",self.suminsured)
		print("\n\t\t\t\t\tEXCESS: 10% of any 1 claim.")
		print("\n\t\t\t\t\tMAIN CONTACT: ",self.contact)
		print("\n\t\t\t\tRISKS: ")
		for risk in self.risks:
			risk.displayRiskDetails()


def addPolicy():
	printlogo("logo.txt")
	print("\n\n\n\t\t\t\t\t\t\t\t\tWELCOME TO PIXEL INSURANCE")
	name=input("\n\n\t\t\t\t\t\t\tENTER THE NEW INSURED'S NAME:")
	name=name.upper()
	address=input("\n\t\t\t\t\t\t\tENTER THE INSURED'D ADDRESS:")
	newpolicy=policy(name,address)
	options=["SET CURRENCY","SET CONTACT","ADD RISK","DISPLAY WARRANTY","ABOUT POLICY","PROCEED TO CONTINUE"]
	while True:
		option=menu(options)
		if(option==1):
			newpolicy.setCurrency()
		elif option==2:
			newpolicy.setcontact()
		elif option==3:
			newpolicy.addRisk()
		elif option==4:
			displayWarranties()
		elif option==5:
			aboutPolicy()
		elif option==6:
			break
		else:	
			print("INVALID OPTION! TRY AGAIN")
		temp=input("\n\n\t\t\t\t\t\t\tPRESS ENTER TO CONTINUE. . . .")
	print("THANK YOU "+name+" FOR SIGNING UP FOR OUR FIRE AND ALLIED PERIL POOLICY")
	newpolicy.display()
	policies.append(newpolicy)


def viewClient():
	printlogo("logo.txt")
	key=input("\n\n\t\t\t\t\t\t\tENTER THE CLIENT'S NAME: ")
	key=key.upper()
	for policy in policies:
		if key in policy.insured:
			policy.display()
			break

def viewCustomers():
	printlogo("logo.txt")
	for policy in policies:
		print('\n\n\n')
		policy.display()		

def main():
	option=0
	while(True):
		try:
			option=menu(main_options)
		except:
			print("PLEASE CHOOSE A VALID OPTION!")
			continue
		if option==1:
			addPolicy()
		elif option==2:
			viewClient()
		elif option==3:
			viewCustomers()
		elif option==4:
			aboutPolicy()
		elif option==5:
			displayWarranties()
		elif option==6:
			break
		else:
			print("INVALID OPTION! TRY AGAIN")
		temp=input("\n\n\t\t\t\t\t\t\tPRESS ENTER TO CONTINUE. . .")


if __name__=="__main__":
	main()